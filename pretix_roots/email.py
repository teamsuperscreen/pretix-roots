import logging
from django.dispatch import receiver


from django.utils.translation import ugettext_lazy as _

from pretix.base.email import TemplateBasedMailRenderer
from pretix.base.signals import register_html_mail_renderers

logger = logging.getLogger(__name__)


class SuperscreenMailRenderer(TemplateBasedMailRenderer):
    verbose_name = _('Roots')
    identifier = 'superscreen'
    thumbnail_filename = 'pretixbase/email/thumb.png'
    template_name = 'pretix_roots/email/plainwrapper.html'


@receiver(register_html_mail_renderers, dispatch_uid="pretixbase_email_renderers")
def base_renderers(sender, **kwargs):
    return [SuperscreenMailRenderer]




