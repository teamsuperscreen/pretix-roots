# pretix-roots

Template plugin with Roots style for pretix ticket system

```html
<img src="{% static "pretix_roots/email/logo-roots-hamburg.png" %}" class="logo"/>


```  

```shell
git tag -a v1.2.8 -m "Releasing version v1.2.8"
```


ja, das müsste dann als User garbe-pretix ausgeführt werden.
Mit sudo den User wechseln `sudo -u garbe-pretix -i`.


#### INSTALL ####
````shell
ssh superscreen@loki.superscreen.de -p 22

www-data@libra:~$ sudo -u garbe-pretix -i
(venv) garbe-pretix@loki:~$ source /var/www/pretix/venv/bin/activate
(venv) garbe-pretix@loki:~$ pip3 install  git+https://git@bitbucket.org/teamsuperscreen/pretix-roots.git
Successfully built pretix-roots
Installing collected packages: pretix-roots
Successfully installed pretix-roots-1.1.8

(venv) garbe-pretix@loki:~$ python -m pretix migrate
(venv) garbe-pretix@loki:~$ python -m pretix rebuild

(venv) garbe-pretix@loki:~$ deactivate

garbe-pretix@loki:~$ sudo service pretix-web restart
garbe-pretix@loki:~$ sudo service pretix-worker restart
````


#### UPDATE ####
````shell
www-data@libra:~$ sudo -u garbe-pretix -i
(venv) garbe-pretix@loki:~$ source /var/www/pretix/venv/bin/activate
(venv) garbe-pretix@loki:~$ pip3 install --upgrade git+https://git@bitbucket.org/teamsuperscreen/pretix-roots.git
(venv) garbe-pretix@loki:~$ python -m pretix migrate
(venv) garbe-pretix@loki:~$ python -m pretix rebuild

(venv) garbe-pretix@loki:~$ deactivate

garbe-pretix@loki:~$ sudo service pretix-web restart
garbe-pretix@loki:~$ sudo service pretix-worker restart
````

 